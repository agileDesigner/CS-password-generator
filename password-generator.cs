using System;
using System.Security.Cryptography;

public class passwordGenerator
{
	static uint getLength()
	{
		string inputLength;
		do
		{
			Console.Write("Password length: ");
			inputLength = Console.ReadLine();
		}while(!uint.TryParse(inputLength, out uint length));
		return length;
	}

	static bool getBool(string question)
	{
		string inputBool;
		while(true)
		{
			Console.Write("{0}? [y/n]: ", question);
			inputBool = Console.ReadLine().ToLower();
			if(inputBool == "y")
			{
				return true;
			}
			if(inputBool == "n")
			{
				return false;
			}
			Console.WriteLine("Enter either 'y' (yes) or 'n' (no)");
		}
	}

	static string getAvaliableCharacters()
	{
		bool includeUppercase = getBool("Uppercase");
		bool includeNumbers = getBool("Numbers");
		bool includeSymbols = getBool("Symbols");

		string avaliableCharacters = "abcdefghijklmnopqrstuvwxyz";
		if(includeUppercase)
		{
			avaliableCharacters += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}
		if(includeNumbers)
		{
			avaliableCharacters += "0123456789";
		}
		if(includeSymbols)
		{
			avaliableCharacters += "`~!@#$%^&^*()-_+={}[]:|;'\"\\<>?/.,<>";
		}
		return avaliableCharacters;
	}

	public static void Main(string[] args)
	{
		uint length = getLength();
		string avaliableCharacters = getAvaliableCharacters();
		int noChars = avaliableCharacters.Length;
		string password = "";
		for(int c = 0; c < length; c++)
		{
			password += avaliableCharacters[RNGCryptoServiceProvider.GetInt32(noChars)];
		}
		Console.WriteLine(password);
	}
}
